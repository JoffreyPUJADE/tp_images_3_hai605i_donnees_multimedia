#include <stdio.h>
#include "image_ppm.h"
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>

template<typename T>
std::string type2String(T valeur)
{
	std::ostringstream oss;
	
	oss << valeur;
	
	return oss.str();
}

std::string nomFichierSansExtension(std::string nomFichier)
{
	std::string res = "";
	size_t index = 0;
	
	while(nomFichier[index] != '.')
	{
		res += nomFichier[index];
		
		++index;
	}
	
	return res;
}

int main(int argc, char* argv[])
{
	char cNomImgLue[250];
	int nH, nW, nTaille;
	
	if (argc != 2) 
	{
		printf("Usage: ImageIn.pgm \n"); 
		exit(1);
	}
	
	sscanf(argv[1], "%s", cNomImgLue);
	
	OCTET *ImgIn;
	
	lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
	nTaille = nH * nW;
	
	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
	
	int nbOccurences[256] = { 0 };
	
	for(int i=0;i<nTaille;++i)
	{
		++nbOccurences[((int)ImgIn[i])];
	}
	
	free(ImgIn);
	
	std::string nomFichierResultats = (nomFichierSansExtension(type2String<char*>(cNomImgLue)) + ".dat");
	
	std::ofstream fluxEcriture;
	
	fluxEcriture.open(nomFichierResultats);
	
	if(!fluxEcriture.is_open())
	{
		std::ostringstream ossErr;
		
		ossErr << "ERREUR : Impossible d'ecrire dans le fichier \"" << nomFichierResultats << "\".";
		
		throw std::runtime_error(ossErr.str().c_str());
	}
	
	for(int i=0;i<256;++i)
		fluxEcriture << i << " " << nbOccurences[i] << ((i < (256 - 1)) ? "\n" : "");
	
	fluxEcriture.close();
	
	return 1;
}

