#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>

template<typename T>
std::string type2String(T valeur)
{
	std::ostringstream oss;
	
	oss << valeur;
	
	return oss.str();
}

template<typename T>
T string2Type(std::string valeur)
{
	std::istringstream iss(valeur);
	
	T res;
	
	iss >> res;
	
	return res;
}

std::string nomFichierSansExtension(std::string nomFichier)
{
	std::string res = "";
	size_t index = 0;
	
	while(nomFichier[index] != '.')
	{
		res += nomFichier[index];
		
		++index;
	}
	
	return res;
}

int main(int argc, char* argv[])
{
	char cNomImgLue[250];
	char typeChoix;
	int indice;
	int nH, nW, nTaille;
	
	if (argc != 4) 
	{
		printf("Usage: ImageIn.pgm c/l indice \n"); 
		exit(1);
	}
	
	sscanf(argv[1], "%s", cNomImgLue);
	typeChoix = argv[2][0];
	indice = string2Type<int>(type2String<char*>(argv[3]));
	
	OCTET *ImgIn;
	
	lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
	nTaille = nH * nW;
	
	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
	
	std::vector<OCTET> tabTemp;
	
	for (int i=0; i < nH; i++)
	{
		for (int j=0; j < nW; j++)
		{
			if(typeChoix == 'c')
			{
				if(j == indice)
					tabTemp.push_back(ImgIn[i]);
			}
			else if(typeChoix == 'l')
			{
				if(i == indice)
					tabTemp.push_back(ImgIn[j]);
			}
		}
	}
	
	std::vector<OCTET> tabProfil;
	
	for(size_t i=0;i<tabTemp.size();++i)
	{
		tabProfil.push_back(tabTemp[i]);
	}
	
	free(ImgIn);
	
	std::ostringstream ossNFR;
	
	ossNFR << nomFichierSansExtension(type2String<char*>(cNomImgLue)) << "_" << typeChoix << "_" << indice << ".dat";
	
	std::string nomFichierResultats = ossNFR.str();
	
	std::ofstream fluxEcriture;
	
	fluxEcriture.open(nomFichierResultats);
	
	if(!fluxEcriture.is_open())
	{
		std::ostringstream ossErr;
		
		ossErr << "ERREUR : Impossible d'ecrire dans le fichier \"" << nomFichierResultats << "\".";
		
		throw std::runtime_error(ossErr.str().c_str());
	}
	
	for(size_t i=0;i<tabTemp.size();++i)
	{
		std::cout << i << " " << tabProfil[i] << ((i < (tabTemp.size() - 1)) ? "\n" : "");
		fluxEcriture << i << " " << tabProfil[i] << ((i < (tabTemp.size() - 1)) ? "\n" : "");
	}
	
	fluxEcriture.close();
	
	return 1;
}

