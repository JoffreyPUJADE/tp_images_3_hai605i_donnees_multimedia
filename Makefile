ifeq ($(OS),Windows_NT)
	Linux=no
	UNAME_S = Windows_NT
else
	UNAME_S=$(shell uname -s)
endif

ifeq ($(UNAME_S),Linux)
	Linux=yes
endif

DirSrc=src
DirObj=obj

Compiler=g++
Link=-o
Preproc=-c -g -Wall -Werror -Wextra -Wfatal-errors -fexceptions -O3
PreprocFlags=
LinkFlags=$(PreprocFlags)

ifeq ($(Linux),yes)
	PreprocSysFlags=
	LinkSysFlags=$(PreprocSysFlags)
	RM=rm -f
	cmdClean=$(RM) $(DirObj)/*.o
	Extension=
else
	PreprocSysFlags=
	LinkSysFlags=$(PreprocSysFlags)
	RM=del
	cmdClean=$(RM) $(DirObj)\*.o
	Extension=.exe
endif

Src=$(wildcard $(DirSrc)/*.cpp)

Obj=$(patsubst $(DirSrc)/%.cpp,$(DirObj)/%.o,$(Src))

Execs=$(patsubst $(DirSrc)/%.cpp,%$(Extension),$(Src))

all: $(DirObj) $(Execs)
default: $(DirObj) $(Execs)

$(Execs): $(Obj)
	$(Compiler) $(Link) $@ $(addprefix $(DirObj)/,$(addsuffix .o,$(basename $(@F)))) $(LinkFlags) $(LinkSysFlags)

$(DirObj)/%.o: $(DirSrc)/%.cpp | $(DirObj)
	$(Compiler) $(Link) $@ $< $(Preproc) $(PreprocFlags) $(PreprocSysFlags)

$(DirObj):
	mkdir $@

.PONY: clean mrproper run

clean:
	$(cmdClean)

mrproper:
	$(RM) $(Execs)


